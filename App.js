import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import AppStack from './Src/Nav/Main'

const App = () => {
  return(
    <AppStack />
  );
}

export default App;