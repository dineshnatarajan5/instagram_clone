import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Dimensions
} from 'react-native';

const {height, width} = Dimensions.get('window');

export default class Splash extends Component{
    render(){
        return(
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle={'light-content'} backgroundColor={'#000000'} />
                <View style={styles.logo_cover}>
                    <Image style={styles.logo} source={require('../../assets/instagram.png')} />
                </View>
                <View style={styles.bottom}>
                    <Text style={styles.txt}>from</Text>
                    <Text style={styles.txt_com}>FACEBOOK</Text>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#191B1B'
    },
    logo_cover:{
        height:110,
        width:110
    },
    logo:{
        flex:1,
        height:null,
        width: null
    },
    bottom:{
        position:'absolute',
        bottom:40,
        right:0,
        left:0,
        alignItems:'center'
    },
    txt:{
        color:'#8A8A8A'
    },
    txt_com:{
        color:'red'
    },
});