import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
  Dimensions, Platform
} from 'react-native';

import {Header, Divider} from 'react-native-elements'

const {height, width} = Dimensions.get('window');

const tabIcon = (props) =>(
    props.focused ? 
    <Image style={{height:30, width:30}} source={require('../../assets/Icons/home_after.png')} /> :
    <Image style={{height:30, width:30}} source={require('../../assets/Icons/home_before.png')} /> 
);

export default class Home extends Component{
    constructor(props){
        super(props)
      }
    static navigationOptions = {
        tabBarIcon: tabIcon
    };
    render(){
        console.log(height,Platform.OS);
        return(
            <View style={styles.container}>
                <StatusBar backgroundColor={'#F0F0F0'} barStyle={'dark-content'} />
                <Header
                 backgroundColor={'white'}
                 placement='left'
                 leftComponent={()=>
                     <View style={styles.hdr_camera_cover}>
                         <Image style={styles.hdr_camera} source={require('../../assets/Icons/camera.png')} />
                     </View>
                 }
                 centerComponent={()=>
                    <View style={styles.hdr_name_cover}>
                        <Image style={styles.hdr_camera} source={require('../../assets/instagram-name-logo.png')} />
                    </View>}
                 rightComponent={()=>
                    <View style={styles.hdr_camera_cover}>
                        <Image style={styles.hdr_camera} source={require('../../assets/Icons/message.png')} />
                        <View style={styles.notification}>
                            <Text style={{color:'white', fontSize: 10}}>3</Text>
                        </View>
                    </View>
                }
                />
                <View style={styles.content}>
                    <View style={styles.status}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={styles.user_cover}>
                                <View style={styles.user}>
                                    <Image style={styles.img} source={require('../../assets/profile_img.png')} />
                                    <View style={styles.add}>
                                        <View style={styles.plus}>
                                            <Image style={styles.hdr_camera} source={require("../../assets/Icons/plus.png")} />
                                        </View>
                                    </View>
                                </View>
                                <Text>Your Story</Text>
                            </View>
                        </ScrollView>
                    </View>
                    <Divider style={{height:2,backgroundColor:'#E9E9E9'}} />
                    <View style={styles.post}></View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    icn:{
        height:30, 
        width:30
    },
    container:{
        flex:1,
    },
    hdr_camera_cover:{
        height: 35,
        width: 35,
    },
    hdr_camera:{
        flex:1,
        height: null,
        width: null,
    },
    notification:{
        backgroundColor:'#ED4956',
        position:'absolute',
        top:0,
        right:0,
        height:18,
        width:18,
        borderRadius:9,
        justifyContent:'center',
        alignItems:'center'
    },
    hdr_name_cover:{
        height:40, 
        width: 100
    },
    status:{
        height: height/7.466666667,
        flexDirection:'row',
    },
    user_cover:{
        width: 100,
        justifyContent:'space-evenly',
        alignItems:'center'
    },
    user:{
        height: height/12.8,
        width:height/12.8,
        borderRadius: height/25.6
    },
    img:{
        flex:1,
        height: null,
        width: null,
        borderRadius: height/25.6
    },
    add:{
        height:25,
        width:25,
        borderRadius:13,
        position:'absolute',
        bottom:0,
        right:0,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: '#3698F2',
        borderWidth:2,
        borderColor:'white'
    },
    plus:{
        height:15,
        width:15,
    },
});