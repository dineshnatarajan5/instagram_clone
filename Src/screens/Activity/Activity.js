import React, { Component } from 'react';
import {
  SafeAreaView,
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

const tabIcon = (props) =>(
  props.focused ? 
  <Image style={{height:30, width:30}} source={require('../../assets/Icons/heart_after.png')} /> :
  <Image style={{height:30, width:30}} source={require('../../assets/Icons/heart_before.png')} /> 
);
export default class Activity extends Component {
  constructor(props){
    super(props)
  }
  static navigationOptions = {
    tabBarIcon : tabIcon
  };
  render(){
    return(
      <View>
          <Text>Activity</Text>
      </View>
    )
  }
}


const styles = StyleSheet.create({
});