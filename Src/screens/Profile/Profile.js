import React, { Component } from 'react';
import {
  SafeAreaView,
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

const tabIcon = (props) =>(
  props.focused ? (
    <View style={{height:30, width:30, justifyContent:'center', alignItems:'center', borderRadius: 15, borderWidth:2}}>
      <Image style={{height:24, width:24, borderRadius: 12}} source={require('../../assets/profile_img.png')} /> 
    </View>
  )
  :
  <Image style={{height:30, width:30, borderRadius: 15}} source={require('../../assets/profile_img.png')} /> 
);

export default class Profile extends Component {
  constructor(props){
    super(props)
  }
  static navigationOptions ={
    tabBarIcon : tabIcon,
  };
  render(){
    return(
      <View>
          <Text>Search</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
});