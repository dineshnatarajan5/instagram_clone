import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  StatusBar,
} from 'react-native';

const tabIcon = (props) =>(
  props.focused ? 
  <Image style={{height:30, width:30}} source={require('../../assets/Icons/search_after.png')} /> :
  <Image style={{height:30, width:30}} source={require('../../assets/Icons/search_before.png')} /> 
);

export default class Search extends Component {
  constructor(props){
    super(props)
  }
  static navigationOptions = {
    tabBarIcon : tabIcon,
  };
  render(){
    return(
      <View>
          <Text>Search</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  icn:{
    height:30, 
    width:30
},

});
