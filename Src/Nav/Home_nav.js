import { add } from 'react-native-reanimated';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Activity from '../screens/Activity/Activity';
import Add from '../screens/Add/Add';
import Home from '../screens/Home/Home';
import Profile from '../screens/Profile/Profile';
import Search from '../screens/Search/Search';


const Home_nav = createBottomTabNavigator({
    home: {
        screen: Home,
    },
    search: {
        screen: Search,
    },
    add: {
        screen: Add, 
    },
    activity: {
        screen: Activity,
    },
    profile:{
        screen: Profile
    },
},{
    tabBarOptions: {
        showLabel: false,
    },
});

export default Home_nav;