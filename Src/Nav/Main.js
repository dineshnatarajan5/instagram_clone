import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';

import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import Splash from '../screens/Splash/Splash';
import Home_nav from './Home_nav';

export default AppStack = createAppContainer(createSwitchNavigator({
    splash : Splash ,
    Home : Home_nav,
},{
    initialRouteName:'Home'
}));